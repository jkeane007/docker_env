
server {
    listen       80;
    server_name d-kingsnz.justin.exp.local;

    access_log /var/log/nginx/kingsnz.access_log;
    # error_log /var/log/nginx/kingsnz.error_log;
    rewrite_log on;

    root /apps/tigerz11;

    #Rules for the default location (equivalent of public_html)
    location / {
        index index.html index.php; ## Allow a static html file to be shown first
        try_files $uri $uri/ @handler; ## If missing pass the URI to Magento's front handler
        expires 30d; ## Assume all files are cachable
    }

    ## These locations would be hidden by .htaccess normally
    location /app/                { deny all; }
    location /includes/           { deny all; }
    location /lib/                { deny all; }
    location /lib/minify/         { allow all; }  ## Deny is applied after rewrites so must specifically allow minify
    location /media/downloadable/ { deny all; }
    location /pkginfo/            { deny all; }
    location /report/config.xml   { deny all; }
    location /var/                { deny all; }

    location @handler { ## Magento uses a common front handler
        rewrite / /index.php;
    }

    #cache all static content for at least 2 days
    location ~ .*.(js|css|gif|jpg|jpeg|png|swf|bmp)$ {
        try_files $uri @handler; ## If missing pass the URI to Magento's front handler
        expires 7d;
    }

    location ~ \.php/ { ## Forward paths like /js/index.php/x.js to relevant handler
        rewrite ^(.*\.php)/ $1 last;
    }

    location ~ \.php$ {
        fastcgi_pass php5.6:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;

        include        fastcgi_params;

        fastcgi_param  MAGE_RUN_TYPE   website;
        fastcgi_param  MAGE_RUN_CODE   adventure_kings_nz;
        fastcgi_read_timeout    300;

    }
}
