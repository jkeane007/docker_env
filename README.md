MYSQL
Create crontab on client machine and run scripts in container from there:
/usr/bin/docker exec -u jkeane supa_php5.6 sh /apps/tools.emg/Symfony/bin/fixgrid.sh

PHP5.6
 - Set 'always_populate_raw_post_data' to '-1' in cli php.ini
   - PHP message: PHP Deprecated:  Automatically populating $HTTP_RAW_POST_DATA is deprecated and will be removed in a future version. To avoid this warning set 'always_populate_raw_post_data' to '-1' in php.ini